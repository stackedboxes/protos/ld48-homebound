extends Node

# The game over causes.
enum GameOverCause {
	ENERGY_LOW = 0,
	ENERGY_HIGH,
	LIFE_SUPPORT_LOW,
	LIFE_SUPPORT_HIGH,
	REASON_LOW,
	REASON_HIGH
}

# Emitted when the player dies.
# Args: cause: int
signal gameOvered


# Emitted when the player reaches Earth.
# Args: None
signal reachedEarth


# Emitted to make encounters progress.
# Args: None
signal encounterTicked


# Emitted when an enconter finishes.
# Args: None
signal encounterDone


# Emitted when the player selects an option in the Options Dialog.
# Args: choice: int
signal optionSelected
