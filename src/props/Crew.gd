extends "res://props/Human.gd"

onready var uniformColors = [ "dc3c3c", "dcc73c", "63dc3c", "3c59dc" ]

func _ready() -> void:
	._ready()
	$Body.self_modulate = RNG.drawOne(uniformColors)
