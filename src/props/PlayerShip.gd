extends Node2D


func turnEnginesOn() -> void:
	$Flames.visible = true


func turnEnginesOff() -> void:
	$Flames.visible = false
