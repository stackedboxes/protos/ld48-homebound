extends Node2D


onready var colors = [
	"d06262", "d0bc62", "9bd062", "62d06e", "62d09a", "62ced0", "629cd0",
	"6f62d0", "ae62d0", "d062c7", "d06298",	"50665e", "666450", "665450",
	"505266", "acacac", "474747",
]


func _ready() -> void:
	$Sprite.self_modulate = RNG.drawOne(colors)
