extends Node2D

onready var bodyColors = [
	"d59898", "d5c798", "c3d598", "a5d598", "98d5aa", "98d5cb", "98c4d5",
	"98add5", "9f98d5", "c698d5", "d598c3", "d598ac", "a6a6a6", "373737",
]

onready var skinColors = [
	"f9e8d0", "c4b39c", "887c6a", "e8d8c0", "fffde1", "f1e5ca", "faf0fb",
]

onready var hairColors = [
	"a1a1a1", "52514f", "181818", "53342a", "53412a", "483011", "362004",
	"e1d771", "a9a251", "726c34", "453b1b", "261f08", "392b2a",
]

onready var hairStyles = [
	load("res://props/HumanHair1.png"),
	load("res://props/HumanHair2.png"),
	load("res://props/HumanHair3.png"),
	load("res://props/HumanHair4.png"),
	load("res://props/HumanHair5.png"),
]

func _ready() -> void:
	$Body.self_modulate = RNG.drawOne(bodyColors)
	$Head.self_modulate = RNG.drawOne(skinColors)
	$Head/Hair.texture = RNG.drawOne(hairStyles)
	$Head/Hair.self_modulate = RNG.drawOne(hairColors)
