extends Control


func _onBummerButtonPressed() -> void:
	SceneStack.pop()


func onPush(cause: int) -> void:
	$Description.text = descFromCause(cause)


func descFromCause(cause: int) -> String:
	match cause:
		GameEvents.GameOverCause.ENERGY_LOW:
			return "The ship energy levels fell to zero. All systems are down. And you let everyone down."
		GameEvents.GameOverCause.ENERGY_HIGH:
			return "The ship energy levels raised to dangerous level. And the raised more. All systems overloaded. Electric shocks, explosions."
		GameEvents.GameOverCause.LIFE_SUPPORT_LOW:
			if RNG.flipCoin():
				return "Without life support the ship cannot, well, support life. Basically, you all asphyxiated."
			else:
				return "Life support in a space ship can mean several things including, in your specific case, feeding the crew. Yup, you all starved to death."
		GameEvents.GameOverCause.LIFE_SUPPORT_HIGH:
			if RNG.flipCoin():
				return "Unstable life support systems made the air pressure inside the ship grow boundlessly. One by one, you and your crew were crushed by the artificial atmosphere. I am sorry for you and for whoever will have to clean up this mess."
			else:
				return "Life support in a space ship can mean several things. Good things, I must say. Fancy food, top quality entertainment, scented candles. You just had too much of it. The crew got into an hedonistic frenzy. The ship went out of control. Unstable as it was, this quickly snowballed to tragedy."
		GameEvents.GameOverCause.REASON_LOW:
			if RNG.flipCoin():
				return "Commanding a crew that cannot think is like playing a game with a disconnected controller. Well, in the case of the controller, you can actually get some fun if you don't notice it. I remember once when I was a kid and... oh, sorry, I digress. Well, an unstable and uncontrollable ship is no fun. Ever."
			else:
				return "Your crew mutinied! This must be because they couldn't reason anymore, right? Reasonable people would not stage a mutiny against you, right? You are a great captain, right? (Either way, you have been ejected outside of the ship. It was funnier in Among Us.)"
		GameEvents.GameOverCause.REASON_HIGH:
			if RNG.flipCoin():
				return "Your crew got more and more reasonable. Super smart people, no doubt. Superhuman analytical capabilities. And suddenly, nothing more was being done in the ship. You sadly witness your unstable ship heading to disaster, with two words on your mind: Analysis Paralysis!"
			else:
				return "As the crew levels of rationality grew higher and higher, deep philosophical discussions started to happen on every corridor. Nobody was doing what they were supposed to do anymore. The ship, unstable as it was, will soon be lost. But some folks on deck 42 have finally found out if that witnessless falling tree made noise."
		_:
			return "" # Can't happen
