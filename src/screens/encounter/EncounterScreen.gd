extends Control

func onPush(encounterScene) -> void:
	GameEvents.connect("encounterDone", self, "_onEncounterDone")
	add_child(encounterScene)


func _onEncounterDone() -> void:
	SceneStack.pop()
