extends Control

# UI elements
export(NodePath) var energyLinePath
onready var energyLine: Line2D = get_node(energyLinePath)

export(NodePath) var lifeSupportLinePath
onready var lifeSupportLine: Line2D = get_node(lifeSupportLinePath)

export(NodePath) var reasonLinePath
onready var reasonLine: Line2D = get_node(reasonLinePath)

var alertingE := false
var alertingL := false
var alertingR := false

# Constants related to the graphing area
const GA_WIDTH := 1260.0
const GA_HEIGHT := 180.0

const ALERT_LEVEL_LOW := 17
const ALERT_LEVEL_HIGH := 83

func addPoint() -> void:
	var t := GameState.time / 100.0
	var x :=  t * GA_WIDTH
	var d = -GA_HEIGHT / 100.0

	var y = d * GameState.energy
	energyLine.add_point(Vector2(x, y))

	y = d * GameState.lifeSupport
	lifeSupportLine.add_point(Vector2(x, y))

	y = d * GameState.reason
	reasonLine.add_point(Vector2(x, y))

	$KeyBG/Energy/Value.text = "%.1f  Δ%.1f" % [ GameState.energy, GameState.dEnergy ]
	$KeyBG/LifeSupport/Value.text = "%.1f  Δ%.1f" % [ GameState.lifeSupport, GameState.dLifeSupport ]
	$KeyBG/Reason/Value.text = "%.1f  Δ%.1f" % [ GameState.reason, GameState.dReason ]

	if !alertingE && (GameState.energy > ALERT_LEVEL_HIGH || GameState.energy < ALERT_LEVEL_LOW):
		alertingE = true
		$AlarmAudioPlayer.play()
		$KeyBG/AnimationPlayerE.play("alert")
	elif alertingE && (GameState.energy < ALERT_LEVEL_HIGH && GameState.energy > ALERT_LEVEL_LOW):
		alertingE = false
		$KeyBG/AnimationPlayerE.play("idle")

	if !alertingL && (GameState.lifeSupport > ALERT_LEVEL_HIGH || GameState.lifeSupport < ALERT_LEVEL_LOW):
		alertingL = true
		$AlarmAudioPlayer.play()
		$KeyBG/AnimationPlayerL.play("alert")
	elif alertingL && (GameState.lifeSupport < ALERT_LEVEL_HIGH && GameState.lifeSupport > ALERT_LEVEL_LOW):
		alertingL = false
		$KeyBG/AnimationPlayerL.play("idle")

	if !alertingR && (GameState.reason > ALERT_LEVEL_HIGH || GameState.reason < ALERT_LEVEL_LOW):
		alertingR = true
		$AlarmAudioPlayer.play()
		$KeyBG/AnimationPlayerR.play("alert")
	elif alertingR && (GameState.reason < ALERT_LEVEL_HIGH && GameState.reason > ALERT_LEVEL_LOW):
		alertingR = false
		$KeyBG/AnimationPlayerR.play("idle")
