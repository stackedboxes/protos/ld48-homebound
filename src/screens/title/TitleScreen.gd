extends Control

func _ready() -> void:
	SceneStack.setInitialScene(self)

func _onPlayButtonPressed() -> void:
	SceneStack.push("res://screens/instructions/InstructionsScreen.tscn")
