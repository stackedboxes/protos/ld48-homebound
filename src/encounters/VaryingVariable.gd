extends Node2D


func _ready() -> void:
	$AudioPlayer.play()
	$AnimationPlayer.connect("animation_finished", self, "queue_free")
	position = Vector2(RNG.uniform(100, 1180), RNG.uniform(100, 500))

func initialize(variable: String, amount: float) -> void:
	var varSign := "+" if amount > 0.0 else "-"
	var formattedAmount := "%s%.1f" % [ varSign, abs(amount)]

	var label := ""
	var color := "#ffffff"

	# Stringly typed code ahead!
	match variable:
		"e":
			label = "Energy"
			color = "#e63939"
		"de":
			label = "ΔEnergy"
			color = "#e63939"
		"l":
			label = "Life Support"
			color = "#39e639"
		"dl":
			label = "ΔLife Support"
			color = "#39e639"
		"r":
			label = "Reason"
			color = "#399ee6"
		"dr":
			label = "ΔReason"
			color = "#399ee6"

	$Label.text = "%s %s" % [ label, formattedAmount ]
	$Label["custom_colors/font_color"] = color
