extends "res://encounters/base/LandEncounter.gd"


func _ready() -> void:
	text('Your sensor detect large concentrations of oil on this planet.')
	yield(GameEvents, "encounterTicked")
	text('A landing party goes to the surface to check it.')
	$Crew.visible = true
	yield(GameEvents, "encounterTicked")
	text('And indeed, this planet is a huge source of oil.')
	yield(GameEvents, "encounterTicked")

	offerChoice("Drill some oil?",
				"Yes, we are energy hungry!",
				"We don't need no oil!")
	yield(GameEvents, "encounterTicked")

	match(GameState.choice):
		0:
			text('The landing party quickly drills a lot of oil using your ship\'s high-tech gear. Oil is such an ancient fuel, but still works on the 23rd century if you can find some.')
			changeEnergy(+2)
		1:
			$Crew.visible = false
			text('The landing party returns to your carbon-neutral ship.')

	yield(GameEvents, "encounterTicked")

	done()
