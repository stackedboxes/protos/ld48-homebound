extends "res://encounters/base/LandEncounter.gd"


func _ready() -> void:
	text("You remember this planet. It used have several tasty animals. What about hunting some of them?")
	yield(GameEvents, "encounterTicked")

	offerChoice("Hunt some tasty animals?",
				"Yes, make primitive traps using materials we find right here!",
				"Yes, hunt them with some high-powered laser weapons!",
				"No, we already have plenty of food.")
	yield(GameEvents, "encounterTicked")

	match(GameState.choice):
		0:
			text('The crew enjoyed the hunt. We didn\'t hunt much, but the primitive trap making activity make their minds think about different things for a while.')
			changeReason(+1.5)
			changeLifeSupport(+2.0)
		1:
			text('Ah, nothing like modern hunting. As long as the batteries are full, you get them!')
			changeEnergy(-3.5)
			changeLifeSupport(+5.0)
		2:
			text('No hunting today!')

	yield(GameEvents, "encounterTicked")
	text('You just enjoy the views for some time then return to the ship')
	yield(GameEvents, "encounterTicked")

	done()
