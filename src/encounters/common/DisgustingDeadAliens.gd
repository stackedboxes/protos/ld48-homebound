extends "res://encounters/base/AlienShipEncounter.gd"

func _ready() -> void:
	text("Since you could not get any radio response you decided to board the ship.")
	yield(GameEvents, "encounterTicked")
	text("That's what people do when they find an alien ship in the deep space, right?")
	yield(GameEvents, "encounterTicked")
	text("You start exploring the exotic rooms.")
	yield(GameEvents, "encounterTicked")
	$Aliens.visible = true
	text("Until you find large auditorium filled with the ugliest aliens you ever saw!")
	sound("bad_surprise")
	yield(GameEvents, "encounterTicked")
	text("This view will impair your thinking for a long time.")
	changeDReason(-0.25)
	yield(GameEvents, "encounterTicked")
	offerChoice("What do you do?",
				"Run for your lives!!!",
				"Fight!")
	yield(GameEvents, "encounterTicked")

	match(GameState.choice):
		0:
			text("You run without looking back. Phew!")
			$Aliens.visible = false
			yield(GameEvents, "encounterTicked")
			done()
		1:
			fight()


func fight() -> void:
	text("You and your crew fight bravely.")
	for alien in $Aliens.get_children():
		sound("laser")
		alien.rotate(PI/2)
		yield(get_tree().create_timer(0.25), "timeout")
	text("Until no alien is standing.")
	yield(GameEvents, "encounterTicked")

	text("These dead aliens are even more disgusting! Yikes!")
	changeReason(-3.0)
	yield(GameEvents, "encounterTicked")

	text("All the laser shooting damaged the alien ship. It's even more unstable than your own!")
	sound("alarm")
	$Aliens.visible = false
	yield(GameEvents, "encounterTicked")

	text("But you see some alien tech you could use.")
	yield(GameEvents, "encounterTicked")

	offerChoice("Take something before leaving?",
				"Take an Oxy-Gen(TM), a ship atmosphere improver",
				"Take some energy-hungry decoration",
				"Take an alien body. (Just why?!)",
				"Just leave")
	yield(GameEvents, "encounterTicked")

	match(GameState.choice):
		0:
			text("OK, this will improve your life support systems.")
			changeDLifeSupport(0.45)
		1:
			text("Alrighty, they aren't particularly pretty, but they sure drain a lot of current.")
			changeDEnergy(-0.3)
		2:
			text("Well, you are the boss. But do you think people will be able to reason with this THING around?")
			changeDReason(-0.35)
		3:
			text("Better safe than sorry. You rush back to your ship.")


	yield(GameEvents, "encounterTicked")
	done()
