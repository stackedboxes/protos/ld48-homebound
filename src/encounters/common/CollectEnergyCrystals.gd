extends "res://encounters/base/LandEncounter.gd"


func _ready() -> void:
	text('Your sensor detect an unusually high amount of energy crystals on this planet.')
	yield(GameEvents, "encounterTicked")
	text('A landing party goes to the surface to check it.')
	$YSort/Crew.visible = true
	yield(GameEvents, "encounterTicked")
	text('Even expecting to find lots of energy crystals, the landing party is in awe! That\'s a LOT of crystals!')
	yield(GameEvents, "encounterTicked")

	offerChoice("Collect all crystals you can get?",
				"Yes, we are energy hungry!",
				"No! We are scientists, not New Age screwballs!")
	yield(GameEvents, "encounterTicked")

	match(GameState.choice):
		0:
			text('The landing party grabs loads of crystals and brings them to the ship.')
			$YSort/Crystals.visible = false
			$YSort/Crew.visible = false
			changeEnergy(+5)
		1:
			$YSort/Crew.visible = false
			text('The landing party returns bringing only memories.')

	yield(GameEvents, "encounterTicked")

	done()
