extends Encounter


func _ready() -> void:
	text('The vessel hails you through the intercom: "May the truth be with you, friends! Come in, let\'s talk!"')
	yield(GameEvents, "encounterTicked")
	text('You cannot refuse to see some friendly fellow earthlings, so you teleport into the ship.')
	$Folks/Crew.visible = true
	yield(GameEvents, "encounterTicked")
	text('The owner of the voice you heard a while back on the intercom welcomes you.')
	$Folks/Philosphers/Human2.visible = true
	yield(GameEvents, "encounterTicked")
	text('"Hello, friend! This is a civilian ship. All we passengers happen to be philosophers!"')
	yield(GameEvents, "encounterTicked")
	text("Now, that's something you don't see everyday in space! Your host continues:")
	yield(GameEvents, "encounterTicked")
	text('"We are just about to have dinner. Would you like to join us?"')
	yield(GameEvents, "encounterTicked")

	offerChoice("Have dinner with the philosophers?",
				"I never refuse free food!",
				"Politely refuse.")
	yield(GameEvents, "encounterTicked")

	match(GameState.choice):
		0:
			for phi in $Folks/Philosphers.get_children():
				phi.visible = true
			text('You all sit around the table. Apparently there is a shortage of cutlery on board, so you have to share forks with your table neighbors.')
			yield(GameEvents, "encounterTicked")
			text('All in all, an intellectually instigating meal.')
			changeReason(+4)
			changeDReason(+0.1)
			yield(GameEvents, "encounterTicked")
			text('Despite these weirdos joking all the time about starving because the fork is being used by someone else.')
		1:
			text('The philosphers understand that you must be in a hurry and urge you to remain in the path of truth and reason.')

	yield(GameEvents, "encounterTicked")

	done()
