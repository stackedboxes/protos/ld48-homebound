extends "res://encounters/base/AlienShipEncounter.gd"

func _ready() -> void:
	text("You try to contact the alien space station, but get no response.")
	yield(GameEvents, "encounterTicked")
	text("Intrigued by the exotic lines of this humongous structure, you decide to board it.")
	$Crew.visible = true
	yield(GameEvents, "encounterTicked")
	text("You look around and see nothing interesting. You explore rooms and corridors of unusual shapes.")
	yield(GameEvents, "encounterTicked")
	text("Then you stumble upon someone... human!")
	$Human.visible = true
	yield(GameEvents, "encounterTicked")
	text('"Oh, I must be the luckiest person in the universe! Years ago my ship had problems and drifted through space..."')
	yield(GameEvents, "encounterTicked")
	text('You interrupt to say that this doesn\'t sound like a lucky event.')
	yield(GameEvents, "encounterTicked")
	text('"Right, sir. But after months drifting I stumbled upon this abandoned alien space station. It had all I needed to keep alive, but I thought I\'d eventually die here. But now you found me!"')
	yield(GameEvents, "encounterTicked")
	text('Here\'s someone expecting to be rescued. Because that\'s what we do with the earthlings we find, right? He continues:')
	yield(GameEvents, "encounterTicked")
	text('"Please take me with you. I can work!"')

	yield(GameEvents, "encounterTicked")

	offerChoice("So, what kind of work you'll assign to this lucky person?",
				"Solar panel janitoring.",
				"Water the plants.",
				"You are our guest! You don't need to work, just eat our food and breathe our air!")
	yield(GameEvents, "encounterTicked")

	match(GameState.choice):
		0:
			text('Your new crewmember will keep the solar panels clean and functional!')
			changeDEnergy(+0.4)
		1:
			text('Your new crewmember will keep the ship plants green.')
			changeDLifeSupport(+0.35)
		2:
			text('Think about an amused person!')
			changeDLifeSupport(-0.4)

	yield(GameEvents, "encounterTicked")
	text('You resume your voyage with one more person on board!')
	yield(GameEvents, "encounterTicked")

	done()
