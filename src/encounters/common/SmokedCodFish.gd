extends "res://encounters/Encounter.gd"


func _ready() -> void:
	text("After exchanging pleasantries through the radio, you disembark. The station's second officer welcomes you:")
	yield(GameEvents, "encounterTicked")
	text('"Hello, fellow humans! Long time I don\'t see a ship this large!"')
	yield(GameEvents, "encounterTicked")
	text('Someone is cooking nearby. You could bet it\'s fried chicken. You ask: "Officer, you sure have a great cook here! The smell is delicious!"')
	yield(GameEvents, "encounterTicked")
	text('The officer frowns: "*Sigh.* Yeah the cook is great, but we cannot stand eating chicken anymore! That\'s all we have in stock. Oh, we\'d give anything for some smoked cod!"')
	yield(GameEvents, "encounterTicked")
	text('What a coincidence! You happen to have several casks of smoked cod on the ship. You ask: "You said anything?"')
	yield(GameEvents, "encounterTicked")

	offerChoice("What do you want in exchange for some of your smoked codfish?",
				"Never mind. That smoked cod is MINE.",
				"A solar panel",
				"Some ship-sized heat sinks",
				"Some holo-disks with recordings from late-20th century TV shows.",
				"The Complete Works of Bertrand Russel")
	yield(GameEvents, "encounterTicked")

	match(GameState.choice):
		0:
			text('The space station officer sighs with sorrow.')
		1:
			text('The station crew kindly install your new panels.')
			changeDEnergy(0.3)
			changeLifeSupport(-2.0)
		2:
			text('The station crew kindly install your new heat sinks.')
			changeDEnergy(-0.35)
			changeLifeSupport(-2.0)
		3:
			text('You wonder what kind of stuff people watched on TV.')
			changeDReason(-0.4)
			changeLifeSupport(-2.0)
		4:
			text('Your crew will love the new books!')
			changeDReason(0.3)
			changeLifeSupport(-2.0)

	yield(GameEvents, "encounterTicked")
	text('You bid farewell and return to the ship.')
	yield(GameEvents, "encounterTicked")

	done()
