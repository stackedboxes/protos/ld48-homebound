extends "res://encounters/base/LandEncounter.gd"


func _ready() -> void:
	text("Either Earth has moved light-years away from its expected position or this is an enormous coincidence, a perfect double.")
	yield(GameEvents, "encounterTicked")
	text("Intrigued, you beam down with a support party.")
	$Crew.visible = true
	yield(GameEvents, "encounterTicked")
	text("Once you arrive, it becomes clear this is no Earth. It doesn't even look like Earth from down here. How come?!")
	yield(GameEvents, "encounterTicked")
	text("You walk around for hours. Everthying you see looks primitive. You see someone coming.")
	yield(GameEvents, "encounterTicked")
	text("It's a primitive human, with primitive clothes. But somehow he makes an impression.")
	$FirstHuman.visible = true
	yield(GameEvents, "encounterTicked")
	text('You understand his words: "We can make you see, we can make you hear, we can make you feel."')
	yield(GameEvents, "encounterTicked")
	text('His lips don\'t move. Is this telepathy? Your mind struggles to understand.')
	changeDReason(-0.15)
	yield(GameEvents, "encounterTicked")
	text('He keeps talking. Talking? Whatever. "We like you. We can give anything you want."')
	yield(GameEvents, "encounterTicked")
	text('What does he mean by "we"?')
	yield(GameEvents, "encounterTicked")
	changeDReason(-0.15)
	for human in $Humans.get_children():
		sound("appear")
		human.visible = true
		yield(get_tree().create_timer(0.15), "timeout")

	text("Is this even real? In your state of confusion you cannot even think of something good to ask for. You suppose it's worth making a wish anyway.")
	yield(GameEvents, "encounterTicked")

	offerChoice("What do you ask for?",
				"A really big furnace.",
				"A powerful refrigerator.",
				"A giant chocolate sundae.",
				"A huge compressed coil.")
	yield(GameEvents, "encounterTicked")

	match(GameState.choice):
		0:
			text('With the furnace you can burn organic matter and turn it into energy.')
			changeDEnergy(+0.3)
			changeDLifeSupport(-0.3)
		1:
			text('The refirgerator consumes a lot of power, but keeps your food fresh.')
			changeDEnergy(-0.3)
			changeDLifeSupport(+0.3)
		2:
			text('Yummy! Yummy! Yummy!')
			changeLifeSupport(3.0)
		3:
			text('In a weird way, this thing stores a lot of energy!')
			changeEnergy(3.0)

	yield(GameEvents, "encounterTicked")
	$Humans.visible = false
	$FirstHuman.visible = false
	text("This is all mad. Let us get out of here!")
	yield(GameEvents, "encounterTicked")

	done()
