extends "res://encounters/base/AlienShipEncounter.gd"

func _ready() -> void:
	text("As you approach the ship, you notice it is sending a distress signal.")
	yield(GameEvents, "encounterTicked")
	text('"We are desperate. All our food supply spoiled while we passed through a nebula. We are starving!"')
	yield(GameEvents, "encounterTicked")
	text('You recognize these aliens an allies of Earth. You immediatelly send a small cargo vessel filled with nourishment.')
	changeLifeSupport(-4.2)
	yield(GameEvents, "encounterTicked")
	text('The alien commander cries while he thanks you: "We don\'t have much, but please take something as a token of our gratitude."')
	yield(GameEvents, "encounterTicked")

	offerChoice("What do you take?",
				"An AI autonomous core",
				"A transcalaphonic power generator",
				"Being heroes is all we need!")
	yield(GameEvents, "encounterTicked")

	match(GameState.choice):
		0:
			text('It consumes power, but has some sharp thinking!')
			changeDReason(+0.4)
			changeDEnergy(-0.35)
		1:
			text("This marvel of alien engineering generates power from instabilities in the space-time. But the buzz it makes drives you crazy!")
			changeDReason(-0.35)
			changeDEnergy(+0.35)
		2:
			text('They play the Superman theme as you close the comm channels.')

	yield(GameEvents, "encounterTicked")
	text('Earth, here we go!')
	yield(GameEvents, "encounterTicked")

	done()
