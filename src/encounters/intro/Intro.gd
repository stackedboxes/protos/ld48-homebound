extends "res://encounters/Encounter.gd"


func _ready() -> void:
	$PlayerShip.turnEnginesOff()

	text('Your first-mate enters the bridge exultantly: "Captain, I am happy to confirm that we completed our five-year misson!"')
	yield(GameEvents, "encounterTicked")
	text('"All systems are nominal and stable. We are ready to get back home!"')
	yield(GameEvents, "encounterTicked")
	text('Suddenly, all screens show an alert in big red letters: "Incoming meteor shower!"')
	sound("alarm")
	yield(GameEvents, "encounterTicked")

	text("There's no time to react. Everything starts to shake. You hold firm on you armchairs while your fist mate tries to keep her balance.")
	sound("explosion")
	$AnimationPlayer.play("Shake")
	changeDLifeSupport(RNG.uniform(-0.15,+0.15))
	changeDReason(RNG.uniform(-0.15,+0.15))
	changeDEnergy(RNG.uniform(-0.15,+0.15))
	yield(GameEvents, "encounterTicked")

	$AnimationPlayer.stop()
	text("Soon enough, things are calm again. You ask for a damage report.")
	yield(GameEvents, "encounterTicked")
	text('"Captain, we were hit hard. Systems are holding, but unstable. We are ready to jump into hyperspace, but this will be a rough ride!"')
	yield(GameEvents, "encounterTicked")
	text('You say what spaceship captains say: "Engage!"')
	yield(GameEvents, "encounterTicked")

	$PlayerShip.turnEnginesOn()
	$Starfield.speed = 1.0
	$EngineAudioPlayer.play()

	yield(get_tree().create_timer(2.5), "timeout")

	done()
