extends ColorRect


func _process(_delta: float) -> void:
	$Energy/Value.text = "%.1f  Δ%.1f" % [ GameState.energy, GameState.dEnergy ]
	$LifeSupport/Value.text = "%.1f  Δ%.1f" % [ GameState.lifeSupport, GameState.dLifeSupport ]
	$Reason/Value.text = "%.1f  Δ%.1f" % [ GameState.reason, GameState.dReason ]
