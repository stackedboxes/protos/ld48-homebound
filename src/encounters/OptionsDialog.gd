extends PopupPanel

onready var buttons := [
	$MarginContainer/VBox/Button0,
	$MarginContainer/VBox/Button1,
	$MarginContainer/VBox/Button2,
	$MarginContainer/VBox/Button3,
	$MarginContainer/VBox/Button4,
]


func setLabel(text: String) -> void:
	$MarginContainer/VBox/Label.text = text


func setOptions(options: Array) -> void:
	for i in range(5):
		if len(options) <= i:
			buttons[i].visible = false
		else:
			buttons[i].visible = true
			buttons[i].text = options[i]


func _onButtonPressed(which: int) -> void:
	$SelectionAudioPlayer.play()
	GameEvents.emit_signal("optionSelected", which)
	hide()
