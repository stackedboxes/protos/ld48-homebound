extends Node2D
class_name Encounter

# AKA "quick and dirty game ballancing"
const VAR_CHANGE_MULTIPLIER := 3.0
const VAR_CHANGE_MULTIPLIER_DELTA := 4.2

# A node that will be displayed in the Travel Screen to represent the encounter.
export(PackedScene) var travelScreenScene

# Some text to display in in the Travel Screen to introduce the encounter.
export(String) var travelScreenText = "Something interesting here..."


onready var varyingVariableScene = preload("res://encounters/VaryingVariable.tscn")


# To control encounter ticking. If true, next event will hide the text and tick.
var isShowingText = false


func _ready() -> void:
	GameEvents.connect("optionSelected", self, "_onOptionSelected")


# Show some text.
func text(t: String) -> void:
	$Text.text = t
	$Text.visible = true
	isShowingText = true


func sound(s: String) -> void:
	var audioStream = load("res://sfx/" + s + ".wav")
	$AudioPlayer.stream = audioStream
	$AudioPlayer.play()


# Selected choice will be at `GameState.choice`.
func offerChoice(label: String, option0: String, option1: String,
	option2: String = "", option3: String = "", option4: String = "") -> void:

	yield(get_tree(), "idle_frame") # avoid processing enqued events too soon

	$OptionsDialog.setLabel(label)

	var options = [option0, option1]
	if option2 != "":
		options.push_back(option2)
		if option3 != "":
			options.push_back(option3)
			if option4 != "":
				options.push_back(option4)
	$OptionsDialog.setOptions(options)

	$OptionsDialog.popup_centered()


# Call this to end the encounter.
func done() -> void:
	GameEvents.emit_signal("encounterDone")


# Change this variable by `amount`.
func changeEnergy(amount: float) -> void:
	amount *= VAR_CHANGE_MULTIPLIER
	GameState.energy += amount
	var vv = varyingVariableScene.instance()
	vv.initialize("e", amount)
	add_child(vv)


# Change this variable by `amount`.
func changeDEnergy(amount: float) -> void:
	amount *= VAR_CHANGE_MULTIPLIER_DELTA
	GameState.dEnergy += amount
	var vv = varyingVariableScene.instance()
	vv.initialize("de", amount)
	add_child(vv)


# Change this variable by `amount`.
func changeLifeSupport(amount: float) -> void:
	amount *= VAR_CHANGE_MULTIPLIER
	GameState.lifeSupport += amount
	var vv = varyingVariableScene.instance()
	vv.initialize("l", amount)
	add_child(vv)


# Change this variable by `amount`.
func changeDLifeSupport(amount: float) -> void:
	amount *= VAR_CHANGE_MULTIPLIER_DELTA
	GameState.dLifeSupport += amount
	var vv = varyingVariableScene.instance()
	vv.initialize("dl", amount)
	add_child(vv)


# Change this variable by `amount`.
func changeReason(amount: float) -> void:
	amount *= VAR_CHANGE_MULTIPLIER
	GameState.reason += amount
	var vv = varyingVariableScene.instance()
	vv.initialize("r", amount)
	add_child(vv)


# Change this variable by `amount`.
func changeDReason(amount: float) -> void:
	amount *= VAR_CHANGE_MULTIPLIER_DELTA
	GameState.dReason += amount
	var vv = varyingVariableScene.instance()
	vv.initialize("dr", amount)
	add_child(vv)


func _onOptionSelected(which: int) -> void:
	GameState.choice = which
	GameEvents.emit_signal("encounterTicked")


func _input(event: InputEvent) -> void:
	if !isShowingText:
		return

	var e = event as InputEventKey
	if e != null && e.pressed && !e.echo && [KEY_ENTER, KEY_KP_ENTER, KEY_SPACE].has(e.scancode):
		isShowingText = false
		$Text.visible = false
		GameEvents.emit_signal("encounterTicked")
		return

	e = event as InputEventMouseButton
	if e != null && e.pressed:
		isShowingText = false
		$Text.visible = false
		GameEvents.emit_signal("encounterTicked")
		return
