extends "res://encounters/Encounter.gd"


onready var bgbgbgTints = [
	"afbeba", "beb1af", "bdbeaf", "afbebe", "b0afbe", "b7afbe", "bdafbe",
	"beafb4", "f1b59a", "f1d59a", "f1f09a", "ccf19a", "9af1d3", "9af1e7",
	"9addf1", "9aaef1", "af9af1", "e09af1", "f19abf", "f19aa3",
]

onready var bgbgbgs = [
	preload("res://encounters/BGBGBG_Atmosphere1.png"),
	preload("res://encounters/BGBGBG_Atmosphere2.png")
]

onready var bgbgs = [
	preload("res://encounters/BGBG_Hills.png"),
	preload("res://encounters/BGBG_Mountains.png"),
	preload("res://encounters/BGBG_Trees.png"),
]

onready var bgs = [
	preload("res://encounters/BG_Floor_Grassland.png"),
	preload("res://encounters/BG_Floor_Rocks.png"),
	preload("res://encounters/BG_Floor_Sand.png"),
]


func _ready() -> void:
	._ready()
	$BGBGBG.modulate = RNG.drawOne(bgbgbgTints)
	$BGBGBG.texture = RNG.drawOne(bgbgbgs)
	$BGBG.texture = RNG.drawOne(bgbgs)
	$BG.texture = RNG.drawOne(bgs)
