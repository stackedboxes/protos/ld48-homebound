# LD 49: Homeward

My entry for Ludum Dare 49; the theme was "Unstable".

License is GPL3.

## Credits

* Engine: [Godot](http://godotengine.org) 3.3.3.
* Fonts:
    * [Orbitron](https://www.theleagueofmoveabletype.com/orbitron), by [Matt
      McInerney](http://pixelspread.com), SIL Open Font License 1.1.
    * [Roboto](https://github.com/googlefonts/roboto), originally designed by
      Christian Robertson.
* Graphics:
    * [Krita](https://krita.org) 4.4.8.
    * [Inkscape](https://inkscape.org) 1.1.1.
* Sound:
    * [SFXR](http://www.drpetter.se/project_sfxr.html).
    * [Audacity](https://www.audacityteam.org) 2.4.1.
* Music: [1BITDRAGON](https://1bitdragon.itch.io/1bitdragon).

## Notes to self

### Variables to stabilize

* **Energy.** If too low, ship becomes nonoperational. If too high, systems
  overcharge.
    * Red: e63939
    * HSV: 0, 75, 90
    * RGB: 230, 57, 57
* **Reason.** If too low, crew gets mad or mutinous. If too high, crew gets
  analysis paralysis and nothing gets done.
    * Blue: 399ee6
    * HSV: 205, 75, 90
    * RGB: 57, 128, 230
* **Life support.** If too low, people starve or suffocate. If too high, air
  pressure may crush people; or everyone gets into an hedonistic frenzy and the
  ship's control is lost.
    * Green: 39e639
    * HSV: 120, 75, 90
    * RGB: 57, 230, 57

### UI

* Game window: 1280 x 720
* Preferred button size: 150 x 45

### Messages

* A large rocky spheroid orbiting a star...
* A small tellurian spaceship...
* A weird-looking space station...
* A spaceship of the alien kind...
* This surely looks like a planet...
