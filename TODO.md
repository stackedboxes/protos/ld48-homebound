# TODO

* Implement special encounters for the critical moments.
* Implement many "generic" encounters.
    * Alien university
* Bug: There is some race condition that allow an encounter to be started right
  after `isEndingTrip` is set and the "Oh, no!" message is displayed.
* Fade between different music tracks
* Allow to skip the intro (remember to still cause the initial damage!)
* Improve music, maybe more songs.

## Done

* Avoid repeating encounters.
* Make and integrate Instructions Screen.
* Implement reaching Earth!
* Implement many "generic" encounters.
    * Fake Earth
    * Drill oil
    * Drill energy crystal
    * Disgusting dead aliens
    * Hunting grounds
    * Alien arcade
    * Dump planet
* Add sounds (when selecting options, when game overs...)
* Add Music
* Organize encounter files; get encounters from the filesystem.
* Randomize colors of alien ship/base/encounter
* Make the "encounter label" on the TravelScreen show up for longer.
* Randomize hair style
* Fix var change display of negative deltas (must use abs)
* Implement complex encounter.
* Starfield as Travel Screen background.
* Starfield as Title Screen background.
* Show when variables (and deltas) change during encounters
* Visual feedback when a variable becomes critical.
* Aural feedback when a variable becomes critical.
* Add sound playing to encounters.
* Add a "bevel" to the upper part of the Travel Screen. Dashboard-like, you
  know.
* Generate (dummy) encounters periodically. Go to a dummy Encounter Screen that
  changes the deltas randomly.
* Structure/Screens: Title, Travel, Encounters, Instructions, Game Over,
  Success.
* Game State, the three variables.
* Travel with graphics of the three variables.
* Test export.
* Create global events, emit gameOvered when variables reach 0 or 100, change to
  Game Over screen with custom description.
